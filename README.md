# Calc

Incredibly simple command line calculator written in C using the [precedence climbing algorithm](https://eli.thegreenplace.net/2012/08/02/parsing-expressions-by-precedence-climbing) for parsing expressions.
